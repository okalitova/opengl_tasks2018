#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <LightInfo.hpp>
#include <Texture.hpp>

#include <iostream>
#include <vector>
#include <string>


class MazeApplication : public Application {
public:
    MeshPtr _mazeWalls;
    MeshPtr _mazeFloor;
    MeshPtr _mazeCeiling;

    MeshPtr _marker; //Маркер для источника света

    ShaderProgramPtr _shader;
    ShaderProgramPtr _markerShader;

    int _currentMode = 0;
    int _newMode = 0;
    double current_x = 0.5, current_y = 0.15, current_z = 0.7;

    float _lr = 3.0;
    float _phi = 0.0;
    float _theta = glm::pi<float>() * 0.25f;
    LightInfo _light;

    TexturePtr _wallsTexture;
    TexturePtr _floorTexture;
    TexturePtr _ceilingTexture;
    TexturePtr _wallsTextureNorm;
    TexturePtr _floorTextureNorm;
    TexturePtr _ceilingTextureNorm;

    GLuint _sampler;

    void makeScene() override {
        Application::makeScene();

        //Создаем меш with maze
        _mazeWalls = makeMazeWalls(1.0f);
        _mazeWalls->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
        _mazeFloor = makeMazeFloor(1.0f);
        _mazeFloor->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
        _mazeCeiling = makeMazeCeiling(1.0f);
        _mazeCeiling->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));

        _marker = makeSphere(0.01f);

        //Создаем шейдерную программу
        _shader = std::make_shared<ShaderProgram>("494kalitovaData/texture.vert", "494kalitovaData/texture.frag");
        _markerShader = std::make_shared<ShaderProgram>("494kalitovaData/marker.vert", "494kalitovaData/marker.frag");

        // light initializing
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);
        _light.dir = glm::vec3(0.0, 1.0, 0.0);
        _light.angle = 180.0;

        //loading textures
        _wallsTexture = loadTexture("494kalitovaData/wall_2.jpg");
        _floorTexture = loadTexture("494kalitovaData/ceiling_norm.png");
        _ceilingTexture = loadTexture("494kalitovaData/ceiling_norm.png");

        // norm texture
        _wallsTextureNorm = loadTexture("494kalitovaData/wall_2_norm.jpg");
        _floorTextureNorm = loadTexture("494kalitovaData/ceiling.png");
        _ceilingTextureNorm = loadTexture("494kalitovaData/ceiling.png");

        // creating sampler
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    void updateGUI() override {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("Choose maze observation mode", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {
            if (_currentMode == 1) {
                std::string deb =
                        "x: " + std::to_string(dynamic_cast<FreeCameraMover *>(_cameraMover.get())->_pos[0]) + ";"
                        + "y: " + std::to_string(dynamic_cast<FreeCameraMover *>(_cameraMover.get())->_pos[1]) + ";"
                        + "z: " + std::to_string(dynamic_cast<FreeCameraMover *>(_cameraMover.get())->_pos[2]) + ";";
                ImGui::Text(deb.c_str());
            }

            ImGui::RadioButton("outside", &_newMode, 0);
            ImGui::RadioButton("inside", &_newMode, 1);

            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light")) {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

                if (_currentMode == 0) {
                    ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
                    ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                    ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
                }

                ImGui::SliderFloat("angle", &_light.angle, 0.0f, 180.0f);
            }
        }
        ImGui::End();
    }

    void draw() override {
        if (_currentMode == 0 && _newMode == 1) {
            _currentMode = 1;
            _cameraMover = std::make_shared<FreeCameraMover>(0.5f, 0.5f, 0.69f);
        }
        if (_currentMode == 1 && _newMode == 0) {
            _currentMode = 0;
            _cameraMover = std::make_shared<OrbitCameraMover>();
        }

//        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Устанавливаем шейдер.
        _shader->use();

        _shader->setIntUniform("mode", _currentMode);

        //Загружаем на видеокарту значения юниформ-переменные: время и матрицы
        _shader->setFloatUniform("time", (float)glfwGetTime()); //передаем время в шейдер

        //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        if (_currentMode == 1) {
            glm::vec3 fwdDir = dynamic_cast<FreeCameraMover *>(_cameraMover.get())->_pos;
            _light.position = glm::vec3(fwdDir.x, fwdDir.y, fwdDir.z + 0.1);

//            _light.position = glm::vec3(0.5, 1.0, 0.5);
        } else {
            _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        }
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

        _shader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);

        if (_currentMode == 1) {
            glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * dynamic_cast<FreeCameraMover *>(_cameraMover.get())->_rot;
            _light.dir = _camera.viewMatrix * glm::vec4(forwDir, 1.0)  - _camera.viewMatrix * glm::vec4(0.0, 0.0, 0.0, 1.0);
        } else {
            _light.dir = _camera.viewMatrix * glm::vec4(0.0, 1.0, 0.0, 1.0) - _camera.viewMatrix * glm::vec4(0.0, 0.0, 0.0, 1.0);
        }

        _shader->setVec3Uniform("light.dir", _light.dir);
        _shader->setFloatUniform("light.angle", _light.angle / 360 * 2 * 3.1415);

//        std::cout << "angle: " << _light.angle / 360 * 2 * 3.1415 << "\n";
//        std::cout << "light dir: " << _light.dir.x << " " << _light.dir.y << " " << _light.dir.z <<  "\n";
//        std::cout << "light position: " << _light.position.x << " " << _light.position.y << " " << _light.position.z <<  "\n";

        GLuint textureUnitForDiffuseTex = 0;
        glBindTextureUnit(textureUnitForDiffuseTex, _wallsTexture->texture());
        glBindSampler(textureUnitForDiffuseTex, _sampler);
        _shader->setIntUniform("diffuseTex", textureUnitForDiffuseTex);

        GLuint textureUnitForDiffuseTexNorm = 1;
        glBindTextureUnit(textureUnitForDiffuseTexNorm, _wallsTextureNorm->texture());
        glBindSampler(textureUnitForDiffuseTexNorm, _sampler);
        _shader->setIntUniform("normTex", textureUnitForDiffuseTexNorm);

        //Рисуем maze walls меш
        _shader->setMat4Uniform("modelMatrix", _mazeWalls->modelMatrix());
        _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _mazeWalls->modelMatrix()))));
        _mazeWalls->draw();

        glBindTextureUnit(textureUnitForDiffuseTex, _floorTexture->texture());
        glBindTextureUnit(textureUnitForDiffuseTexNorm, _floorTextureNorm->texture());

        //Рисуем maze floor меш
        _shader->setMat4Uniform("modelMatrix", _mazeFloor->modelMatrix());
        _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _mazeFloor->modelMatrix()))));
        _mazeFloor->draw();

        glBindTextureUnit(textureUnitForDiffuseTex, _ceilingTexture->texture());
        glBindTextureUnit(textureUnitForDiffuseTexNorm, _ceilingTextureNorm->texture());

        //Рисуем maze ceiling меш
        _shader->setMat4Uniform("modelMatrix", _mazeCeiling->modelMatrix());
        _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _mazeCeiling->modelMatrix()))));
        _mazeCeiling->draw();

//        Рисуем маркеры для всех источников света
        {
            _markerShader->use();

            _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
            _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
            _marker->draw();
        }

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }
};

int main()
{
    MazeApplication app;
    app.start();

    return 0;
}