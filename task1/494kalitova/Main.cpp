#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <vector>
#include <string>


class MazeApplication : public Application {
public:
    MeshPtr _maze;

    ShaderProgramPtr _shader;

    int _currentMode = 0;
    int _newMode = 0;
    double current_x = 0.5, current_y = 0.15, current_z = 0.7;

    void makeScene() override {
        Application::makeScene();

        //Создаем меш with maze
        _maze = makeMaze(1.0f);
        _maze->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));

        //Создаем шейдерную программу
        _shader = std::make_shared<ShaderProgram>("494kalitovaData/shaderNormal.vert", "494kalitovaData/shader.frag");
    }

    void updateGUI() override {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("Choose maze observation mode", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {
            if (_currentMode == 1) {
                std::string deb =
                        "x: " + std::to_string(dynamic_cast<FreeCameraMover *>(_cameraMover.get())->_pos[0]) + ";"
                        + "y: " + std::to_string(dynamic_cast<FreeCameraMover *>(_cameraMover.get())->_pos[1]) + ";"
                        + "z: " + std::to_string(dynamic_cast<FreeCameraMover *>(_cameraMover.get())->_pos[2]) + ";";
                ImGui::Text(deb.c_str());
            }

            ImGui::RadioButton("outside", &_newMode, 0);
            ImGui::RadioButton("inside", &_newMode, 1);
        }
        ImGui::End();
    }

    void draw() override {
        if (_currentMode == 0 && _newMode == 1) {
            _currentMode = 1;
            _cameraMover = std::make_shared<FreeCameraMover>(0.5f, 0.5f, 0.69f);
        }
        if (_currentMode == 1 && _newMode == 0) {
            _currentMode = 0;
            _cameraMover = std::make_shared<OrbitCameraMover>();
        }

        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Устанавливаем шейдер.
        _shader->use();

        //Загружаем на видеокарту значения юниформ-переменные: время и матрицы
        _shader->setFloatUniform("time", (float)glfwGetTime()); //передаем время в шейдер

        //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Рисуем maze меш
        _shader->setMat4Uniform("modelMatrix", _maze->modelMatrix());

        _maze->draw();
    }
};

int main()
{
    MazeApplication app;
    app.start();

    return 0;
}